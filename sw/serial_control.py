import serial
import argparse

def fpga_reset(serial):
    serial.write(int(0xdeadbeef).to_bytes(4, byteorder='big'))

def fpga_status(serial):
    serial.reset_input_buffer()
    serial.write(int(0xbabecafe).to_bytes(4, byteorder='big'))
    data = serial.read(9)
    shutdown_state = data[8]
    max_average_power = int.from_bytes(data[:-1], byteorder='little')
    return(shutdown_state, max_average_power)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='TDD Frontend Serial Utility Script')
    parser.add_argument('-p', '--port', default='/dev/ttyACM0')
    parser.add_argument('--reset', default=False, action='store_true')
    parser.add_argument('--hex', default=False, action='store_true')

    args = parser.parse_args()

    serial = serial.Serial(args.port, baudrate=9600)

    if args.reset:
        fpga_reset(serial)
    else:
        status = fpga_status(serial)
        print("Shutdown state, max power since last pull")
        if args.hex:
            print(f"{status[0]:02x} {status[1]:08x}")
        else:
            print(f"{status[0]:3d} {status[1]:10d}")
