library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity sync_bit is
    generic (
        g_STAGES : positive
    );
    port (
        i_clk : in std_logic;
        i_bit : in std_logic;
        o_bit : out std_logic
    );
end sync_bit;

architecture sync_bit_arch of sync_bit is
    signal r_sync : std_logic_vector(g_STAGES-1 downto 0) := (others => '0');
begin
    o_bit <= r_sync(g_STAGES-1);

    sync_procedure : process(i_clk)
    begin
        if i_clk'event and i_clk = '1' then
            r_sync(0) <= i_bit;
            for i in 0 to g_STAGES-2 loop
                r_sync(i+1) <= r_sync(i);
            end loop;
        end if;
    end process;
end;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity fifo is
    generic (
        g_DATA_WIDTH : positive;
        g_SIZE : positive
    );
    port (
        i_clk : in std_logic;

        i_write_req : in std_logic;
        i_din : in std_logic_vector(g_DATA_WIDTH-1 downto 0);

        i_read_req : in std_logic;
        o_dout : out std_logic_vector(g_DATA_WIDTH-1 downto 0);

        o_empty : out std_logic;
        o_full : out std_logic
    );
end fifo;

architecture fifo_arch of fifo is
    type t_data is array(0 to g_SIZE-1) of std_logic_vector(g_DATA_WIDTH-1 downto 0);
    subtype t_idx is integer range 0 to g_SIZE-1;

    signal r_data : t_data := (others => (others => '0'));

    signal r_write_idx : t_idx := 0;
    signal r_read_idx : t_idx := 0;

    signal r_empty : boolean := true;
    signal r_full : boolean := false;

    function get_next_idx(
        idx : t_idx) return t_idx is
        variable next_idx : t_idx;
    begin
        if idx = g_SIZE-1 then
            next_idx := 0;
        else
            next_idx := idx + 1;
        end if;

        return next_idx;
    end function;
begin
    o_dout <= r_data(r_read_idx);
    o_empty <= '1' when r_empty else '0';
    o_full <= '1' when r_full else '0';

    fifo_control : process(i_clk)
        variable r_count : natural := 0;
    begin
        if i_clk'event and i_clk = '1' then
            if i_write_req = '1' and not r_full then
                r_data(r_write_idx) <= i_din;
                r_count := r_count + 1;
                r_write_idx <= get_next_idx(r_write_idx);
            end if;

            if i_read_req = '1' and not r_empty then
                r_count := r_count - 1;
                r_read_idx <= get_next_idx(r_read_idx);
            end if;

            r_empty <= r_count = 0;
            r_full <= r_count = g_SIZE;
        end if;
    end process;
end;
