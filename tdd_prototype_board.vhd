library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity tdd_prototype_board is
    port
    (
        ------------ CLOCK ----------
        ADC_CLK_10 : in std_logic;
        MAX10_CLK1_50 : in std_logic;
        MAX10_CLK2_50 : in std_logic;

        ------------ SDRAM ----------
        DRAM_ADDR : out std_logic_vector(12 downto 0);
        DRAM_BA : out std_logic_vector(1 downto 0);
        DRAM_CAS_N : out std_logic;
        DRAM_CKE : out std_logic;
        DRAM_CLK : out std_logic;
        DRAM_CS_N : out std_logic;
        DRAM_DQ : inout std_logic_vector(15 downto 0);
        DRAM_LDQM : out std_logic;
        DRAM_RAS_N : out std_logic;
        DRAM_UDQM : out std_logic;
        DRAM_WE_N : out std_logic;

        ------------ SEG7 ----------
        HEX0 : out std_logic_vector(7 downto 0);
        HEX1 : out std_logic_vector(7 downto 0);
        HEX2 : out std_logic_vector(7 downto 0);
        HEX3 : out std_logic_vector(7 downto 0);
        HEX4 : out std_logic_vector(7 downto 0);
        HEX5 : out std_logic_vector(7 downto 0);

        ------------ KEY ----------
        -- '0' -> pressed, '1' -> released
        KEY : in std_logic_vector(1 downto 0);

        ------------ LED ----------
        LEDR : out std_logic_vector(9 downto 0);

        ------------ SW ----------
        SW : in std_logic_vector(9 downto 0);

        ------------ VGA ----------
        VGA_B : out    std_logic_vector(3 downto 0);
        VGA_G : out    std_logic_vector(3 downto 0);
        VGA_HS : out std_logic;
        VGA_R : out    std_logic_vector(3 downto 0);
        VGA_VS : out std_logic;

        ------------ Accelerometer ----------
        GSENSOR_CS_N : out std_logic;
        GSENSOR_INT : in std_logic_vector(2 downto 1);
        GSENSOR_SCLK : out std_logic;
        GSENSOR_SDI : inout std_logic;
        GSENSOR_SDO : inout std_logic;

        ------------ Arduino ----------
        ARDUINO_IO : inout std_logic_vector(15 downto 0);
        ARDUINO_RESET_N : inout std_logic;

        ------------ GPIO, GPIO connect to GPIO Default ----------
        GPIO : inout std_logic_vector(35 downto 0)
    );
end tdd_prototype_board;

architecture simple of tdd_prototype_board is
    -- If true, enables some behavior useful for debugging.
    -- Disables some safety checks
    constant DEBUG : boolean := false;

    constant pps_sync_length : natural := 5;
    signal master_clk_10mhz : std_logic;
    signal external_clk_10mhz : std_logic;
    signal external_clk_good : std_logic;

    signal radio_rx_indication : std_logic;

    signal signal_LNA_on : std_logic;
    signal signal_PA_on : std_logic;
    signal signal_SW_rx : std_logic;
    signal signal_SW_tx : std_logic;

    signal signal_PA_on_out : std_logic;
    signal pa_on_last_second_indication : std_logic;

    signal ATT_SI : std_logic;
    signal ATT_LE : std_logic;
    signal ATT_CLK : std_logic;

    signal PPS_IN : std_logic;
    signal LNA_REG_EN : std_logic;

    signal rf_power_sensor_en : std_logic;

    signal test_points : std_logic_vector(1 to 4);

    -- RX - steady state RX
    -- TRAN_TO_TX_LNA_TURNOFF - Wait for LNA to turn off
    -- TRAN_TO_TX_RXBIAS_LOW - Wait for RX pin diode bias to go low
    -- TRAN_TO_TX_TXBIAS_HIGH - Wait for TX pin diode bias to go high
    -- TRAN_TO_TX_PA_TURNON - Wait for PA to turn on
    -- TX - steady state TX
    -- TRAN_TO_RX_PA_TURNOFF - Wait for PA to on
    -- TRAN_TO_RX_TXBIAS_LOW - Wait for TX pin diode bias to go low
    -- TRAN_TO_RX_RXBIAS_HIGH - Wait for RX pin diode boas to go high
    -- TRAN_TO_RX_LNA_TURNON - Wait for LNA to turn on
    type break_state_t is (
            INIT,
            RX,
            TRAN_TO_TX_LNA_TURNOFF,
            TRAN_TO_TX_RXBIAS_LOW,
            TRAN_TO_TX_TXBIAS_HIGH,
            TRAN_TO_TX_PA_TURNON,
            TX,
            TRAN_TO_RX_PA_TURNOFF,
            TRAN_TO_RX_TXBIAS_LOW,
            TRAN_TO_RX_RXBIAS_HIGH,
            TRAN_TO_RX_LNA_TURNON);
    signal break_state : break_state_t := INIT;

    signal state_count : integer := 0;

    signal syncd_pps_in : std_logic;
    signal pps_good : std_logic;

    signal startup_over : std_logic := '0';
    signal dl_active : std_logic;
    signal ul_active : std_logic;

    signal attenuation : std_logic_vector(6 downto 0) := "1111111";

    signal sw_disable_pa : std_logic;

    signal pll_locked : std_logic;

    signal adc_response_valid : std_logic;
    signal adc_response_channel : std_logic_vector(4 downto 0);
    signal adc_response_data : std_logic_vector(11 downto 0);

    signal hex_data : std_logic_vector(23 downto 0);

    signal uart_rx : std_logic;
    signal uart_tx : std_logic;
    signal serial_reset_received : std_logic;

    signal power_fifo_sample_in : std_logic_vector(23 downto 0);
    signal power_fifo_sample_out : std_logic_vector(23 downto 0);
    signal power_fifo_rdreq : std_logic;
    signal power_fifo_wrreq : std_logic;
    signal power_fifo_count : std_logic_vector(7 downto 0);
    signal power_bad : std_logic := '0';
    signal sshutdown : std_logic := '0';
    signal average_power : unsigned(63 downto 0) := to_unsigned(0, 64);

    signal key_debounced : std_logic_vector(key'range);

    signal shutdown_state : std_logic_vector(7 downto 0) := std_logic_vector(to_unsigned(0, 8));
begin
    key_debouncers:
    for i in key'range generate
        debouncer:
        entity work.debouncer(behavioral)
            generic map (
                debounce_cycle_bits => 17,
                debounce_cycles => to_unsigned(100000, 17)
            )
            port map (
                reset => '0',
                clk => master_clk_10mhz,
                input => key(i),
                output => key_debounced(i)
            );
    end generate;

    GPIO(20) <= test_points(1);
    GPIO(22) <= test_points(2);
    GPIO(28) <= test_points(3);
    GPIO(30) <= test_points(4);

    test_points(1) <= key_debounced(0);
    test_points(2) <= key_debounced(1);

    rf_power_sensor_en <= '1';

    -- DOC: LED red 0 is the PPS signal
    LEDR(0) <= syncd_pps_in;
    -- DOC: LED red 1 is the external clock good indication (on means good)
    LEDR(1) <= external_clk_good;
    -- DOC: LED red 2 is the startup done LED
    LEDR(2) <= startup_over;
    -- DOC: LED red 4 is the PPS good indication (on means good)
    LEDR(4) <= pps_good;
    -- DOC: LED red 5 is the ul active indicator
    LEDR(5) <= ul_active;
    -- DOC: LED red 6 is the dl active indicator
    LEDR(6) <= dl_active;
    -- DOC: LED red 7 is the power currently bad LED (may turn off without a
    -- DOC: shutdown reset)
    LEDR(7) <= power_bad;
    -- DOC: LED red 8 is the shutdown signal. This is one of the signals that
    -- DOC: can cause the system to be shut down. Should stay on until reset,
    -- DOC: e.g. by pushing a button (see other docs).
    LEDR(8) <= sshutdown;
    -- DOC: LED red 9 is lit if the PA has been on at any point within the last
    -- DOC: second
    LEDR(9) <= pa_on_last_second_indication;

    GPIO(21) <= ATT_SI;
    GPIO(25) <= ATT_LE;
    GPIO(23) <= ATT_CLK;

    GPIO(9) <= not signal_LNA_on; -- LNA control 0
    GPIO(5) <= not signal_LNA_on; -- LNA control 1
    GPIO(7) <= not signal_LNA_on; -- LNA control 2
    GPIO(3) <= not signal_LNA_on; -- LNA control 3
    ARDUINO_IO(13) <= not signal_SW_tx; -- pin switch bias ch1a control
    ARDUINO_IO(12) <= not signal_SW_tx; -- pin switch bias ch1b control
    ARDUINO_IO(11) <= not signal_SW_rx; -- pin switch bias ch2a control
    ARDUINO_IO(10) <= not signal_SW_rx; -- pin switch bias ch2b control

    uart_rx <= ARDUINO_IO(6);
    ARDUINO_IO(5) <= uart_tx;

    signal_PA_on_out <= signal_PA_on and not sw_disable_pa;
    ARDUINO_IO(9) <= signal_PA_on_out; -- power amplifier control 

    -- DOC: Switch 9 disables the PA when in the on position
    sw_disable_pa <= SW(9);

    external_clk_10mhz <= GPIO(0);

    PPS_IN <= ARDUINO_IO(8);
    ARDUINO_IO(15) <= LNA_REG_EN;

    -- TODO GPIO(xxx) <= signal_name, signal_name <= data, organization!
    GPIO(27) <= rf_power_sensor_en;

    clock_ctrl : entity work.clock_ctrl
    port map (
        internal_clk_10mhz => ADC_CLK_10,
        external_clk_10mhz => external_clk_10mhz,
        master_clk_10mhz => master_clk_10mhz,
        pll_locked => pll_locked,
        external_clk_good => external_clk_good
    );

    hex_display : entity work.hex_display
    port map (
        data => hex_data,
        HEX0 => HEX0,
        HEX1 => HEX1,
        HEX2 => HEX2,
        HEX3 => HEX3,
        HEX4 => HEX4,
        HEX5 => HEX5
    );

    power_fifo_inst : work.power_fifo
    port map (
        clock => master_clk_10mhz,
        data => power_fifo_sample_in,
        rdreq => power_fifo_rdreq,
        wrreq => power_fifo_wrreq,
        q => power_fifo_sample_out,
        usedw => power_fifo_count
    );

    pa_led : process(master_clk_10mhz)
        variable pa_off_count : integer := 10e6;
    begin
        if master_clk_10mhz'event and master_clk_10mhz = '1' then
            pa_on_last_second_indication <= '1';

            if signal_PA_on_out = '0' then
                if pa_off_count < 10e6 then
                    pa_off_count := pa_off_count + 1;
                else
                    pa_on_last_second_indication <= '0';
                end if;
            else
                pa_off_count := 0;
            end if;
        end if;
    end process;

    hex_update : process(master_clk_10mhz)
        constant PERIOD : integer := 1e6;

        variable count : integer := PERIOD;
        variable cur_max : unsigned(63 downto 0) := (others => '0');
    begin
        if master_clk_10mhz'event and master_clk_10mhz = '1' then
            if count = 0 then
                hex_data <= std_logic_vector(cur_max(31 downto 8));
                cur_max := average_power;

                count := PERIOD;
            else
                if cur_max < average_power then
                    cur_max := average_power;
                end if;

                count := count - 1;
            end if;
        end if;
    end process;

    rf_power_monitor : process(master_clk_10mhz)
        constant RF_POWER_CHANNEL : std_logic_vector(4 downto 0) := std_logic_vector(to_unsigned(5, 5));
        constant RF_POWER_THRESHOLD : unsigned(63 downto 0) := to_unsigned(16#4a0000#, 64);
        constant AVERAGE_LENGTH : unsigned(7 downto 0) := to_unsigned(167, 8);
        variable sum : unsigned(63 downto 0) := to_unsigned(0, 64);
        variable sample_sq : unsigned(23 downto 0);
    begin
        if master_clk_10mhz'event and master_clk_10mhz = '1' then
            power_fifo_wrreq <= '0';
            power_fifo_rdreq <= '0';

            sample_sq := unsigned(adc_response_data) * unsigned(adc_response_data);

            if adc_response_valid = '1' and adc_response_channel = RF_POWER_CHANNEL then
                sum := sum + sample_sq;
                power_fifo_sample_in <= std_logic_vector(sample_sq);
                power_fifo_wrreq <= '1';
            elsif unsigned(power_fifo_count) = AVERAGE_LENGTH then
                average_power <= sum;

                if sum >= RF_POWER_THRESHOLD then
                    power_bad <= '1';
                else
                    power_bad <= '0';
                end if;

                sum := sum - unsigned(power_fifo_sample_out);
                power_fifo_rdreq <= '1';
            end if;
        end if;
    end process;

    analog : entity work.analog
    port map (
        reset => '0',
        clk_10mhz => master_clk_10mhz,
        clk_50mhz => MAX10_CLK1_50,
        pll_locked => pll_locked,
        adc_out_valid => adc_response_valid,
        adc_channel_out => adc_response_channel,
        adc_data_out => adc_response_data
    );

    attenuator : entity work.attenuator
    port map (
        reset => '0',
        clk_10mhz => master_clk_10mhz,
        attenuation => attenuation,
        ATT_SI => ATT_SI,
        ATT_LE => ATT_LE,
        ATT_CLK => ATT_CLK
    );

    uart_control : entity work.uart_control
    port map (
        clk_10mhz => master_clk_10mhz,
        rx => uart_rx,
        tx => uart_tx,
        average_power => average_power,
        shutdown_state => shutdown_state,
        serial_reset_received => serial_reset_received
    );

    pps_monitor : entity work.pps_monitor
    generic map (
        g_SYNC_LENGTH => pps_sync_length
    )
    port map (
        i_clk => master_clk_10mhz,
        i_external_pps => PPS_IN,
        o_pps => syncd_pps_in,
        o_pps_good => pps_good
    );

    control : process(master_clk_10mhz)
        type control_state_t is (
            START,
            ATTENUATOR_START,
            STARTUP,
            IDLE,
            SHUTDOWN);

        constant ATTENUATOR_START_DELAY : integer := 1e4; -- 1ms
        constant STARTUP_DELAY : integer := 5000e4; -- 5000ms

        variable control_state : control_state_t := START;
        variable delay_count : integer := ATTENUATOR_START_DELAY;

        variable old_atten_key : std_logic;
        variable old_shutdown_reset_key : std_logic;

        variable pending_attenuation : std_logic_vector(6 downto 0) := (others => '0');
        variable is_attenuation_pending : boolean := false;
    begin
        if master_clk_10mhz'event and master_clk_10mhz = '1' then
            if delay_count /= 0 then
                delay_count := delay_count - 1;
            end if;

            case control_state is
                when START =>
                    shutdown_state <= std_logic_vector(to_unsigned(0, 8));
                    if delay_count = 0 then
                        attenuation <= SW(6 downto 0);
                        control_state := ATTENUATOR_START;
                        delay_count := ATTENUATOR_START_DELAY;
                        startup_over <= '0';
                    end if;

                when ATTENUATOR_START =>
                    shutdown_state <= std_logic_vector(to_unsigned(0, 8));
                    if delay_count = 0 then
                        control_state := STARTUP;
                        delay_count := STARTUP_DELAY;
                    end if;

                when STARTUP =>
                    shutdown_state <= std_logic_vector(to_unsigned(0, 8));

                    if delay_count = 0 and pps_good = '1' then
                        startup_over <= '1';
                        control_state := IDLE;
                    end if;

                    if DEBUG and delay_count = 0 then
                        startup_over <= '1';
                        control_state := IDLE;
                    end if;

                when IDLE =>
                    shutdown_state <= std_logic_vector(to_unsigned(0, 8));
                    if power_bad = '1' then
                        control_state := SHUTDOWN;
                    end if;

                    if pps_good = '0' then
                        control_state := SHUTDOWN;
                    end if;

                    -- TODO debounce and rising edge
                    -- DOC: Key 0 on push triggers attenuation update from
                    -- DOC: switches
                    if KEY(0) = '0' and old_atten_key = '1' then
                        -- DOC: Switch 6 downto 0 is attenuator value.
                        -- DOC: Switch 6 is 16 dB switch 0 is 0.25 dB
                        attenuation <= SW(6 downto 0);
                    elsif is_attenuation_pending then
                        attenuation <= pending_attenuation;
                        is_attenuation_pending := false;
                    end if;

                when SHUTDOWN =>
                    shutdown_state <= std_logic_vector(to_unsigned(4, 8));
                    sshutdown <= '1';

                    if KEY(0) = '0' and old_atten_key = '1' then
                        pending_attenuation := SW(6 downto 0);
                        is_attenuation_pending := true;
                    end if;

                    -- TODO debounce and rising edge
                    -- DOC: Key 1 resets shutdown conditions (such as over
                    -- DOC: power) on push (not on hold).
                    if (KEY(1) = '0' and old_shutdown_reset_key = '1') or serial_reset_received = '1' then
                        sshutdown <= '0';
                        control_state := IDLE;
                    end if;
            end case;

            old_atten_key := KEY(0);
            old_shutdown_reset_key := KEY(1);
        end if;
    end process;

--    pps_monitor : process(master_clk_10mhz)
--        variable prev_pps : std_logic := '0';
--    begin
--        if master_clk_10mhz'event and master_clk_10mhz = '1' then
--        end if;
--    end process;

--    sync_pps_in:
--    process(master_clk_10mhz)
--        variable sync_register : std_logic_vector(pps_sync_length-1 downto 0);
--    begin
--        if master_clk_10mhz'event and master_clk_10mhz = '1' then
--            syncd_pps_in <= sync_register(pps_sync_length-1);
--            for i in 0 to pps_sync_length-2 loop
--                sync_register(i+1) := sync_register(i);
--            end loop;
--            sync_register(0) := PPS_IN;
--        end if;
--    end process sync_pps_in;

    -- LTE config mode 2 special subframe config 6
    -- TODO: put in separate file
    tdd_timing_process:
    process(master_clk_10mhz)
        -- Values in 10ths of microseconds
        constant tdd_periodicity : unsigned := to_unsigned(50000, 24);

        -- These are the union of the numbers from the magical place we were
        -- told to follow
        constant dl_start_1 : unsigned := to_unsigned(0, 24);
        constant dl_stop_1 : unsigned := to_unsigned(16600, 24);
        constant dl_start_2 : unsigned := to_unsigned(29900, 24);
        constant dl_stop_2 : unsigned := to_unsigned(50000, 24);
        constant ul_start : unsigned := to_unsigned(18200, 24);
        constant ul_stop : unsigned := to_unsigned(29870, 24);

        constant pps_too_short_interval : unsigned(23 downto 0) :=
            to_unsigned(9_000_000, 24);
        constant pps_too_long_interval : unsigned(23 downto 0) :=
            to_unsigned(11_000_000, 24);

        variable count : unsigned(23 downto 0);
        variable count_safeguard : unsigned(23 downto 0);
        variable wait_edge_safeguard : std_logic := '0';
        variable clean_edge_safeguard : std_logic := '0';
        variable old_syncd_pps_in : std_logic;
    begin
        if master_clk_10mhz'event and master_clk_10mhz = '1' then
            count := count + to_unsigned(1, 24);

            if count_safeguard < pps_too_long_interval then
                count_safeguard := count_safeguard + to_unsigned(1, 24);
            end if;

            if syncd_pps_in = '1' and old_syncd_pps_in = '0' then
                count := tdd_periodicity - to_unsigned(pps_sync_length, 24);

                if wait_edge_safeguard = '1' and
                        count_safeguard > pps_too_short_interval then
                    clean_edge_safeguard := '1';
                end if;

                count_safeguard := to_unsigned(0, 24);

                wait_edge_safeguard := '1';
            elsif count >= tdd_periodicity then
                count := to_unsigned(0, 24);
            end if;

            dl_active <= '0';

            if count >= dl_start_1 and count < dl_stop_1 then
                dl_active <= '1';
            end if;

            if count >= dl_start_2 and count < dl_stop_2 then
                dl_active <= '1';
            end if;

            if count >= ul_start and count < ul_stop then
                ul_active <= '1';
            else
                ul_active <= '0';
            end if;

            old_syncd_pps_in := syncd_pps_in;

            -- If off by a million CLEARLY something's wrong
            if count_safeguard >= pps_too_long_interval then
                wait_edge_safeguard := '0';
                clean_edge_safeguard := '0';
            end if;

            -- If we haven't seen a clean edge, we don't know anything
            if clean_edge_safeguard = '0' then
                dl_active <= '0';
                ul_active <= '0';
            end if;
        end if;
    end process tdd_timing_process;

    LNA_REG_EN <= '1';

    --PA_REG_VSEL <= "00";
    --LNA_REG_VSEL <= "00";

    --FRONTEND_GPIO(7 downto 4) <=
        --std_logic_vector(to_unsigned(break_state_t'POS(break_state), 4));
--    FRONTEND_GPIO(15 downto 9) <= std_logic_vector(state_count);

    --CLK_STANDBY <= '0';

    --power_good <= (RF_REG_LDO_4_8_PG
        --AND RF_REG_LDO_3_3VRF_PG
        --AND RF_REG_DCDC_PG
        --AND PA_REG_DCDC_PG
        --AND PA_REG_LDO_PG
        --AND LNA_REG_DCDC_PG
        --AND LNA_REG_LDO_PG);

    -- Toggles led once per second, period = 2s
    clock_process:
    process(master_clk_10mhz)
        variable count : integer := 0;
        variable toggle_output : std_logic := '0';
    begin
        if master_clk_10mhz'event and master_clk_10mhz = '1' then
            if count = 10_000_000 then
                toggle_output := not toggle_output;
                count := 0;
            end if;

            count := count + 1;

            -- DOC: LED red 3 is a blinking output (to show the clock is good)
            LEDR(3) <= toggle_output;
        end if;
    end process clock_process;

    --SDR_GPIO <= (4 => 'Z', others => '0');
    --radio_rx_indication <= SDR_GPIO(4) or (NOT dl_active);
    -- DOC: SW(8)=1 forces rx when SW(7)=1, tx when SW(7)=0
    radio_rx_indication <= (GPIO(12) or (NOT dl_active) or sshutdown) when SW(8) = '0' else SW(7);

    break_before_make_proc:
    process(master_clk_10mhz)
        -- TODO: check datasheet for timing values
        constant LNA_off_delay : integer := 10; -- from old code
        constant LNA_on_delay : integer := 10; -- arbitrary, no delay in old code

        -- From datasheet: https://www.empowerrf.com/datasheet/Empower_RF_Amplifier_1208.pdf
        -- Typ: 2us, Max: 5us
        constant PA_off_delay : integer := 50;
        constant PA_on_delay : integer := 50;

        -- 90% falling time is ~4.4us
        -- on and off are backwards
        constant diode_bias_on_time : integer := 50;
        constant diode_bias_off_time : integer := 25;
        constant rx_to_tx_switch_time : integer := 150;
        constant tx_to_rx_all_off_time : integer := 150;

        variable last_input : std_logic;

        variable pre_sw_in2 : std_logic := '0';
        variable pre_sw_in : std_logic := '0';
        variable rx_indication : std_logic := '0';
    begin
        if master_clk_10mhz'event and master_clk_10mhz = '1' then
            -- Doing this to handle metastability

            rx_indication := pre_sw_in;
            pre_sw_in := last_input;
            last_input := radio_rx_indication;

            if state_count /= 0 then
                state_count <= state_count - 1;
            end if;

            case break_state is
                when INIT =>
                    if startup_over = '1' then
                        break_state <= TRAN_TO_RX_TXBIAS_LOW;
                        -- TODO: go to RX equivalent state
                    else
                        break_state <= INIT;
                    end if;
                when RX =>
                    if rx_indication = '0' then
                        state_count <= LNA_off_delay;
                        break_state <= TRAN_TO_TX_LNA_TURNOFF;
                    else
                        break_state <= RX;
                    end if;

                when TRAN_TO_TX_LNA_TURNOFF =>
                    if rx_indication = '0' then
                        if state_count = 0 then
                            state_count <= rx_to_tx_switch_time;
                            break_state <= TRAN_TO_TX_TXBIAS_HIGH;
                        else
                            break_state <= TRAN_TO_TX_LNA_TURNOFF;
                        end if;
                    else
                        state_count <= LNA_on_delay;
                        break_state <= TRAN_TO_RX_LNA_TURNON;
                    end if;

                when TRAN_TO_TX_RXBIAS_LOW =>
                    -- DEAD STATE
                    if rx_indication = '0' then
                        if state_count = 0 then
                            state_count <= diode_bias_on_time;
                            break_state <= TRAN_TO_TX_TXBIAS_HIGH;
                        else
                            break_state <= TRAN_TO_TX_RXBIAS_LOW;
                        end if;
                    else
                        state_count <= diode_bias_on_time;
                        break_state <= TRAN_TO_RX_RXBIAS_HIGH;
                    end if;

                when TRAN_TO_TX_TXBIAS_HIGH =>
                    if rx_indication = '0' then
                        if state_count = 0 then
                            state_count <= PA_on_delay;
                            break_state <= TRAN_TO_TX_PA_TURNON;
                        else
                            break_state <= TRAN_TO_TX_TXBIAS_HIGH;
                        end if;
                    else
                        state_count <= tx_to_rx_all_off_time;
                        break_state <= TRAN_TO_RX_TXBIAS_LOW;
                    end if;

                when TRAN_TO_TX_PA_TURNON =>
                    if rx_indication = '0' then
                        if state_count = 0 then
                            break_state <= TX;
                        else
                            break_state <= TRAN_TO_TX_PA_TURNON;
                        end if;
                    else
                        state_count <= PA_off_delay;
                        break_state <= TRAN_TO_RX_PA_TURNOFF;
                    end if;

                when TX =>
                    if rx_indication = '1' then
                        state_count <= PA_off_delay;
                        break_state <= TRAN_TO_RX_PA_TURNOFF;
                    else
                        break_state <= TX;
                    end if;

                when TRAN_TO_RX_PA_TURNOFF =>
                    if rx_indication = '1' then
                        if state_count = 0 then
                            state_count <= tx_to_rx_all_off_time;
                            break_state <= TRAN_TO_RX_TXBIAS_LOW;
                        else
                            break_state <= TRAN_TO_RX_PA_TURNOFF;
                        end if;
                    else
                        state_count <= PA_on_delay;
                        break_state <= TRAN_TO_TX_PA_TURNON;
                    end if;

                when TRAN_TO_RX_TXBIAS_LOW =>
                    if rx_indication = '1' then
                        if state_count = 0 then
                            state_count <= diode_bias_on_time;
                            break_state <= TRAN_TO_RX_RXBIAS_HIGH;
                        else
                            break_state <= TRAN_TO_RX_TXBIAS_LOW;
                        end if;
                    else
                        state_count <= diode_bias_on_time;
                        break_state <= TRAN_TO_TX_TXBIAS_HIGH;
                    end if;

                when TRAN_TO_RX_RXBIAS_HIGH =>
                    if rx_indication = '1' then
                        if state_count = 0 then
                            state_count <= LNA_on_delay;
                            break_state <= TRAN_TO_RX_LNA_TURNON;
                        else
                            break_state <= TRAN_TO_RX_RXBIAS_HIGH;
                        end if;
                    else
                        state_count <= diode_bias_off_time;
                        break_state <= TRAN_TO_TX_TXBIAS_HIGH;
                    end if;

                when TRAN_TO_RX_LNA_TURNON =>
                    if rx_indication = '1' then
                        if state_count = 0 then
                            break_state <= RX;
                        else
                            break_state <= TRAN_TO_RX_LNA_TURNON;
                        end if;
                    else
                        state_count <= LNA_off_delay;
                        break_state <= TRAN_TO_TX_LNA_TURNOFF;
                    end if;

                when others =>
                    break_state <= TRAN_TO_RX_TXBIAS_LOW;

            end case;
        end if;
    end process break_before_make_proc;

    process(break_state)
        variable LNA_on : std_logic := '0';
        variable PA_on : std_logic := '0';
        variable SW_rx : std_logic := '0';
        variable SW_tx : std_logic := '0';
    begin
        case break_state is
            when INIT =>
                SW_rx := '0';
                SW_tx := '0';
                LNA_on := '0';
                PA_on := '0';

            when RX =>
                SW_rx := '1';
                SW_tx := '0';
                LNA_on := '1';
                PA_on := '0';

            when TRAN_TO_TX_LNA_TURNOFF =>
                SW_rx := '1';
                SW_tx := '0';
                LNA_on := '0';
                PA_on := '0';

            when TRAN_TO_TX_RXBIAS_LOW =>
                SW_rx := '0';
                SW_tx := '0';
                LNA_on := '0';
                PA_on := '0';

            when TRAN_TO_TX_TXBIAS_HIGH =>
                SW_rx := '0';
                SW_tx := '1';
                LNA_on := '0';
                PA_on := '0';

            when TRAN_TO_TX_PA_TURNON =>
                SW_rx := '0';
                SW_tx := '1';
                LNA_on := '0';
                PA_on := '1';

            when TX =>
                SW_rx := '0';
                SW_tx := '1';
                LNA_on := '0';
                PA_on := '1';

            when TRAN_TO_RX_PA_TURNOFF =>
                SW_rx := '0';
                SW_tx := '1';
                LNA_on := '0';
                PA_on := '0';

            when TRAN_TO_RX_TXBIAS_LOW =>
                SW_rx := '0';
                SW_tx := '0';
                LNA_on := '0';
                PA_on := '0';

            when TRAN_TO_RX_RXBIAS_HIGH =>
                SW_rx := '1';
                SW_tx := '0';
                LNA_on := '0';
                PA_on := '0';

            when TRAN_TO_RX_LNA_TURNON =>
                SW_rx := '1';
                SW_tx := '0';
                LNA_on := '1';
                PA_on := '0';

            when others =>
                SW_rx := '0';
                SW_tx := '0';
                LNA_on := '0';
                PA_on := '0';

        end case;

        signal_LNA_on <= LNA_on;
        signal_PA_on <= PA_on;
        signal_SW_rx <= SW_rx;
        signal_SW_tx <= SW_tx;

    end process;
end;
