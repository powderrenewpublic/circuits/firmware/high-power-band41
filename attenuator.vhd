library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

-- Datasheet: https://www.psemi.com/pdf/datasheets/pe43712ds.pdf

entity attenuator is
    port (
        signal reset : in std_logic;
        signal clk_10mhz : in std_logic;
        signal attenuation : in std_logic_vector(6 downto 0);

        signal ATT_SI : out std_logic;
        signal ATT_LE : out std_logic;
        signal ATT_CLK : out std_logic
    );
end attenuator;

architecture attenuator_arch of attenuator is
    signal clk_5mhz : std_logic;
begin
    five_mhz_process:
    process(clk_10mhz)
        variable state : std_logic := '0';
    begin
        if clk_10mhz'event and clk_10mhz = '1' then
            state := not state;
            clk_5mhz <= state;
        end if;
    end process;

    attenuator_process:
    process(clk_5mhz)
        constant address : std_logic_vector(7 downto 0) := "00000000";
        variable bit_sequence : std_logic_vector(16 downto 0);
        variable le_sequence : std_logic_vector(16 downto 0);
        variable clk_en : std_logic := '0';
        variable prev_attenuation : std_logic_vector(6 downto 0) := "1111111";

        type att_state_t is (
                IDLE,
                SERIAL);
        variable att_state : att_state_t := IDLE;
    begin
        if clk_5mhz'event and clk_5mhz = '0' then
            case att_state is
                when IDLE =>
                    clk_en := '0';

                    if attenuation /= prev_attenuation then
                        prev_attenuation := attenuation;
                        bit_sequence := '0' & address & '0' & attenuation;
                        le_sequence := "10000000000000000";
                        att_state := SERIAL;
                    end if;

                when SERIAL =>
                    if le_sequence(0) = '1' then
                        clk_en := '0';
                        att_state := IDLE;
                    else
                        clk_en := '1';
                    end if;
                    ATT_SI <= bit_sequence(0);
                    ATT_LE <= le_sequence(0);

                    bit_sequence := '0' & bit_sequence(16 downto 1);
                    le_sequence := '0' & le_sequence(16 downto 1);
            end case;
        end if;


        -- This is safe because clk_en transitions on falling edge of clk_5mhz
        -- Any transition in clk_en *should* happen only when clk_5mhz is
        -- already 0, therefore no glitch.
        ATT_CLK <= clk_5mhz and clk_en;
    end process;
end;
