library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;


entity debouncer_tb is
end entity debouncer_tb;

architecture tb of debouncer_tb is
    signal clk : std_logic;
    signal reset : std_logic;
    signal input : std_logic;
    signal output : std_logic;
begin
    dut:
    entity work.debouncer(behavioral)
        generic map (
            debounce_cycle_bits => 3,
            debounce_cycles => to_unsigned(5, 3)
        )
        port map (
            reset => reset,
            clk => clk,
            input => input,
            output => output
        );

    tb:
    process
        constant input_vector : std_logic_vector :=
            "11010100000000111100000000110101000000000";
    begin
        clk <= '0';
        reset <= '1';
        input <= '0';

        wait for 5 ns;

        clk <= '1';
        wait for 5 ns;

        clk <= '0';
        reset <= '0';
        wait for 5 ns;

        for i in input_vector'range loop
            input <= input_vector(i);
            clk <= '0';
            wait for 5 ns;

            clk <= '1';
            wait for 5 ns;
        end loop;

        for i in 0 to 10 loop
            clk <= '0';
            wait for 5 ns;

            clk <= '1';
            wait for 5 ns;
        end loop;

        wait;
    end process tb;
end architecture tb;
