library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library vunit_lib;
context vunit_lib.vunit_context;

entity pps_monitor_tb is
    generic (
        runner_cfg : string
    );
end pps_monitor_tb;

architecture tb of pps_monitor_tb is
    signal r_clk : std_logic := '1';
    signal r_test_external_pps : std_logic := '0';
    signal r_start_pps : boolean := false;
    signal w_pps : std_logic;
    signal w_pps_good : std_logic;
begin
    r_clk <= not r_clk after 50 ns;

    pps_monitor_dut : entity work.pps_monitor
    port map (
        i_clk => r_clk,
        i_external_pps => r_test_external_pps,
        o_pps => w_pps,
        o_pps_good => w_pps_good
    );

    test_runner : process
    begin
        test_runner_setup(runner, runner_cfg);

        while test_suite loop
            if run("no_pps") then
                wait for 1 fs;
                check(w_pps = '0');
                check(w_pps_good = '0');

                wait for 2 sec;

                check(w_pps = '0');
                check(w_pps_good = '0');

            elsif run("pps_good") then
                r_start_pps <= true;
                wait for 1 fs;
                check(w_pps = '0');

                wait for 1 ms;

                wait for 250 ms;
                wait until rising_edge(r_clk);
                check(w_pps = '0');
                check(w_pps_good = '0');

                wait for 250 ms;
                wait until rising_edge(r_clk);
                check(w_pps = '1');
                check(w_pps_good = '0');

                wait for 250 ms;
                wait until rising_edge(r_clk);
                check(w_pps = '1');
                check(w_pps_good = '0');

                wait for 250 ms;
                wait until rising_edge(r_clk);
                check(w_pps = '0');
                check(w_pps_good = '0');

                wait for 250 ms;
                wait until rising_edge(r_clk);
                check(w_pps = '0');
                check(w_pps_good = '0');

                wait for 250 ms;
                wait until rising_edge(r_clk);
                check(w_pps = '1');
                check(w_pps_good = '1');

            elsif run("pps_bad") then
                r_start_pps <= true;

                wait until rising_edge(w_pps);
                wait until rising_edge(w_pps);
                check(w_pps_good = '0');

            end if;

        end loop;

        test_runner_cleanup(runner);
    end process;

    pps_generator : process
    begin
        wait until r_start_pps;

        if running_test_case = "pps_good" then
            for i in 0 to 5 loop
                wait for 500 ms;
                r_test_external_pps <= not r_test_external_pps;
            end loop;
        elsif running_test_case = "pps_bad" then
            for i in 0 to 5 loop
                wait for 400 ms;
                r_test_external_pps <= not r_test_external_pps;
            end loop;
        end if;
    end process;
end;
