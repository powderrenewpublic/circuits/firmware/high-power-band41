library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library vunit_lib;
context vunit_lib.vunit_context;

entity fifo_tb is
    generic (
        runner_cfg : string
    );
end fifo_tb;

architecture tb of fifo_tb is
    signal r_clk : std_logic := '0';
    signal r_write_req : std_logic := '0';
    signal r_din : std_logic_vector(7 downto 0) := (others => '0');
    signal r_read_req : std_logic := '0';
    signal w_dout : std_logic_vector(7 downto 0);
    signal w_empty : std_logic;
    signal w_full : std_logic;
begin
    test_fifo : entity work.fifo
    generic map (
        g_DATA_WIDTH => 8,
        g_SIZE => 256
    )
    port map (
        i_clk => r_clk,
        i_write_req => r_write_req,
        i_din => r_din,
        i_read_req => r_read_req,
        o_dout => w_dout,
        o_empty => w_empty,
        o_full => w_full
    );

    r_clk <= not r_clk after 100 ns;

    test_runner : process
    begin
        test_runner_setup(runner, runner_cfg);

        while test_suite loop
            if run("initial_conditions") then
                check(w_empty = '1', "FIFO expected to start empty");
                check(w_full = '0', "FIFO expected to start empty");

            elsif run("single_insert") then
                wait until falling_edge(r_clk);

                r_din <= x"FF";
                r_write_req <= '1';

                wait until falling_edge(r_clk);

                r_write_req <= '0';
                check(w_empty = '0', "FIFO has one item");
                check(w_full = '0');
                check(w_dout = x"FF");

            elsif run("fill_to_empty") then
                for i in 0 to 16#FF# loop
                    wait until falling_edge(r_clk);
                    if i /= 0 then
                        check(w_empty = '0' and w_full = '0', "Partial fill status");
                    end if;
                    r_din <= std_logic_vector(to_unsigned(i, r_din'length));
                    r_write_req <= '1';
                end loop;

                wait until falling_edge(r_clk);
                r_write_req <= '0';
                check(w_empty = '0', "FIFO shouldn't be empty");
                check(w_full = '1', "FIFO should be full");

                for i in 0 to 16#FF# loop
                    wait until falling_edge(r_clk);
                    if i /= 0 then
                        check(w_empty = '0' and w_full = '0', "Partial empty status");
                    end if;
                    check(w_dout = std_logic_vector(to_unsigned(i, w_dout'length)), "d_out doesn't match");
                    r_read_req <= '1';
                end loop;

                wait until falling_edge(r_clk);
                r_read_req <= '0';
                check(w_empty = '1', "FIFO should be empty");
                check(w_full = '0');
            end if;
        end loop;

        test_runner_cleanup(runner);
    end process;
end;
