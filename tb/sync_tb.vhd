library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library vunit_lib;
context vunit_lib.vunit_context;

entity sync_tb is
    generic (
        runner_cfg : string
    );
end sync_tb;

architecture tb of sync_tb is
    signal r_clk : std_logic := '1';
    signal r_sync_in : std_logic := '0';
    signal w_sync_out : std_logic;
begin
    r_clk <= not r_clk after 50 ns;

    sync_bit_dut : entity work.sync_bit
    generic map (g_STAGES => 3)
    port map (i_clk => r_clk, i_bit => r_sync_in, o_bit => w_sync_out);

    test_runner : process
        variable test_data : std_logic_vector(15 downto 0) := "1010111100001011";
    begin
        test_runner_setup(runner, runner_cfg);

        while test_suite loop
            if run("initial_conditions") then
                check(w_sync_out = '0');

            elsif run("sync_test") then
                for i in 0 to test_data'length-1 loop
                    wait until rising_edge(r_clk);
                    r_sync_in <= test_data(i);

                    wait until falling_edge(r_clk);
                    if i-3 >= 0 then
                        check(w_sync_out = test_data(i-3), "sync");
                    end if;
                end loop;

            end if;
        end loop;

        test_runner_cleanup(runner);
    end process;
end;
