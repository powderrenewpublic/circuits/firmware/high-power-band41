from vunit import VUnit

vu = VUnit.from_argv()
lib = vu.add_library('lib')
lib.add_source_files('../util.vhd')
lib.add_source_files('../pps_monitor.vhd')
lib.add_source_files('*.vhd')
vu.main()
