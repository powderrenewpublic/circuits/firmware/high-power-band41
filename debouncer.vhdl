library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;


entity debouncer is
    generic (
        sync_length : natural := 3;
        debounce_cycle_bits : natural;
        debounce_cycles : unsigned
    );
    port (
        reset : IN std_logic;
        clk : IN std_logic;
        input : IN std_logic;
        output : OUT std_logic
    );
end entity debouncer;

architecture behavioral of debouncer is
    signal syncd_input : std_logic := '0';
begin
    synchronizer_process:
    process(clk)
        variable sync_vector : std_logic_vector(sync_length-1 downto 0) :=
            (others => '0');
    begin
        if clk'event and clk = '1' then
            if reset = '1' then
                sync_vector := (others => '0');
            end if;

            syncd_input <= sync_vector(0);

            for i in 0 to sync_length-2 loop
                sync_vector(i) := sync_vector(i+1);
            end loop;

            sync_vector(sync_length-1) := input;
        end if;
    end process synchronizer_process;

    -- debounce:
    -- aaaabbabbb -> aaaabbbbbb - swallow the a within the b, do not swallow
    --      the bb
    -- Assume that first edges coming along are true (i.e. that if there is an
    --      electrical transition then there is a mechanical one, just that
    --      multiple electrical transitions do not imply more mechanical ones).
    -- When edge comes along, it is immediately accepted, then all future edges
    --      are delayed until the signal has not changed for x cycles.
    debounce_process:
    process(clk)
        variable old_input : std_logic := '0';
        variable last_edge_count : unsigned(debounce_cycle_bits-1 downto 0) :=
                debounce_cycles;
    begin
        if clk'event and clk = '1' then
            if reset = '1' then
                last_edge_count := debounce_cycles;
                output <= '0';
                old_input := '0';
            else
                -- If a change has occured
                if syncd_input /= old_input then
                    -- If we're not in the midst of changes just push it
                    -- through
                    if last_edge_count = to_unsigned(0,
                                                     debounce_cycle_bits) then
                        output <= syncd_input;
                    end if;

                    -- In any case, refresh the count
                    last_edge_count := debounce_cycles;

                -- If no change has occured
                else
                    -- If not yet steady
                    if last_edge_count > to_unsigned(0,
                                                     debounce_cycle_bits) then
                        last_edge_count := last_edge_count - 1;

                    -- If steady
                    else
                        output <= syncd_input;
                    end if;
                end if;

                old_input := syncd_input;
            end if;
        end if;
    end process debounce_process;
end architecture behavioral;
