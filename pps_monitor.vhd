library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

-- Takes in external_pps
-- Puts out external_pps synchronized and checks if the pps signal is present/good
entity pps_monitor is
    generic (
        g_SYNC_LENGTH : positive := 3
    );
    port (
        i_clk : in std_logic;
        i_external_pps : in std_logic;

        o_pps : out std_logic;
        o_pps_good : out std_logic
    );
end pps_monitor;

architecture pps_monitor_arch of pps_monitor is
    signal w_pps : std_logic;
    signal r_pps_good : boolean := false;
begin
    external_pps_sync : entity work.sync_bit
    generic map (g_STAGES => 3)
    port map (i_clk => i_clk, i_bit => i_external_pps, o_bit => w_pps);

    o_pps <= w_pps;
    o_pps_good <= '1' when r_pps_good else '0';

    pps_checker : process(i_clk)
        constant MIN_GOOD_COUNT : natural := integer(10e6 * 0.99);
        constant MAX_GOOD_COUNT : natural := integer(10e6 * 1.01);

        variable r_last_edge_count : natural := 0;
        variable r_pps_del : std_logic := '0';
        variable r_pps_fall_seen : boolean := false;
    begin
        if i_clk'event and i_clk = '1' then
            r_last_edge_count := r_last_edge_count + 1;

            if w_pps /= r_pps_del then
                if w_pps = '1' then
                    -- pps rising edge
                    r_pps_good <= r_pps_fall_seen
                            and MIN_GOOD_COUNT <= r_last_edge_count
                            and r_last_edge_count <= MAX_GOOD_COUNT;
                
                    r_last_edge_count := 0;
                    r_pps_fall_seen := false;
                else
                    -- pps falling edge
                    r_pps_fall_seen := true;
                end if;
            end if;

            r_pps_del := w_pps;
        end if;
    end process;
end;
