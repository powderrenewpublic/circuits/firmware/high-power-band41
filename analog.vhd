library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity analog is
    port (
        signal reset : in std_logic;
        signal clk_10mhz : in std_logic;
        signal clk_50mhz : in std_logic; -- clock_clk signal from adc needs to be at least 25 MHz
        signal pll_locked : in std_logic; 

        signal adc_out_valid : out std_logic;
        signal adc_channel_out : out std_logic_vector(4 downto 0);
        signal adc_data_out : out std_logic_vector(11 downto 0)
    );
end analog;

architecture analog_arch of analog is
    signal adc_command_valid : std_logic;
    signal adc_command_channel : std_logic_vector(4 downto 0);
    signal adc_command_ready : std_logic;
    signal adc_response_valid : std_logic;
    signal adc_response_channel : std_logic_vector(4 downto 0);
    signal adc_response_data : std_logic_vector(11 downto 0);
    signal rdreq_sig : std_logic;
    signal rdempty_sig : std_logic;

    component adc_ip is
    port (
            clock_clk              : in  std_logic                     := 'X';             -- clk
            reset_sink_reset_n     : in  std_logic                     := 'X';             -- reset_n
            adc_pll_clock_clk      : in  std_logic                     := 'X';             -- clk
            adc_pll_locked_export  : in  std_logic                     := 'X';             -- export
            command_valid          : in  std_logic                     := 'X';             -- valid
            command_channel        : in  std_logic_vector(4 downto 0)  := (others => 'X'); -- channel
            command_startofpacket  : in  std_logic                     := 'X';             -- startofpacket
            command_endofpacket    : in  std_logic                     := 'X';             -- endofpacket
            command_ready          : out std_logic;                                        -- ready
            response_valid         : out std_logic;                                        -- valid
            response_channel       : out std_logic_vector(4 downto 0);                     -- channel
            response_data          : out std_logic_vector(11 downto 0);                    -- data
            response_startofpacket : out std_logic;                                        -- startofpacket
            response_endofpacket   : out std_logic                                         -- endofpacket
         );
    end component adc_ip;
begin
    -- ADC channel 1: PA Temperature
    -- ADC channel 2: PA Current
    -- ADC channel 3: LNA Bias
    -- ADC channel 4: LNA V
    -- ADC channel 5: RF Average Power
    -- ADC channel 6: Pin Diode Temperature

    adc_command_valid <= '1';

    -- ADC is configured with a sample rate of 1MHz.
    --`Confirmed by outputing response_valid to oscilloscope and measuring the frequency.
    adc_ctrl : process(clk_50mhz)
        variable channel : unsigned(4 downto 0) := to_unsigned(1, 5);
    begin
        if clk_50mhz'event and clk_50mhz = '1' then
            if adc_command_ready = '1' then
                if channel = to_unsigned(6, 5) then
                    channel := to_unsigned(1, 5);
                else
                    channel := channel + 1;
                end if;
            else
                adc_command_channel <= std_logic_vector(channel);
            end if;
        end if;
    end process;

    fifo_read : process(clk_10mhz)
    begin
        if clk_10mhz'event and clk_10mhz = '1' then
            rdreq_sig <= not rdempty_sig;
            adc_out_valid <= rdreq_sig;
        end if;
    end process;

    -- FIFO for crossing 50mhz clock to 10mhz clock.
    adc_fifo_inst : work.adc_fifo
    port map (
        data(16 downto 12) => adc_response_channel,
        data(11 downto 0) => adc_response_data,
        rdclk => clk_10mhz,
        rdreq => rdreq_sig,
        wrclk => clk_50mhz,
        wrreq => adc_response_valid,
        q(16 downto 12) => adc_channel_out,
        q(11 downto 0) => adc_data_out,
        rdempty => rdempty_sig
    );

    adc : component adc_ip
    port map (
        clock_clk              => clk_50mhz,              --          clock.clk
        reset_sink_reset_n     => not reset,     --     reset_sink.reset_n
        adc_pll_clock_clk      => clk_10mhz,      --  adc_pll_clock.clk
        adc_pll_locked_export  => pll_locked,  -- adc_pll_locked.export
        command_valid          => adc_command_valid,          --        command.valid
        command_channel        => adc_command_channel,        --               .channel
        command_startofpacket  => '0',  --               .startofpacket
        command_endofpacket    => '0',    --               .endofpacket
        command_ready          => adc_command_ready,          --               .ready
        response_valid         => adc_response_valid,         --       response.valid
        response_channel       => adc_response_channel,       --               .channel
        response_data          => adc_response_data,          --               .data
        response_startofpacket => open, --               .startofpacket
        response_endofpacket   => open    --               .endofpacket
    );
end;
