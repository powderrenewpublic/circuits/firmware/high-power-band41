library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity hex_display is
    port (
        data : in std_logic_vector(23 downto 0);

        HEX0 : out std_logic_vector(7 downto 0);
        HEX1 : out std_logic_vector(7 downto 0);
        HEX2 : out std_logic_vector(7 downto 0);
        HEX3 : out std_logic_vector(7 downto 0);
        HEX4 : out std_logic_vector(7 downto 0);
        HEX5 : out std_logic_vector(7 downto 0)
    );
end hex_display;

architecture hex_display_arch of hex_display is
    procedure segment (
        signal digit : in std_logic_vector(3 downto 0);
        signal hex : out std_logic_vector(7 downto 0)
    ) is
    begin
        case digit is
            when x"0" => hex <= "11000000"; -- 0     
            when x"1" => hex <= "11111001"; -- 1 
            when x"2" => hex <= "10100100"; -- 2 
            when x"3" => hex <= "10110000"; -- 3 
            when x"4" => hex <= "10011001"; -- 4 
            when x"5" => hex <= "10010010"; -- 5 
            when x"6" => hex <= "10000010"; -- 6 
            when x"7" => hex <= "11111000"; -- 7 
            when x"8" => hex <= "10000000"; -- 8     
            when x"9" => hex <= "10010000"; -- 9 
            when x"A" => hex <= "10001000"; -- A
            when x"B" => hex <= "10000011"; -- b
            when x"C" => hex <= "11000110"; -- C
            when x"D" => hex <= "10100001"; -- d
            when x"E" => hex <= "10000110"; -- E
            when x"F" => hex <= "10001110"; -- F
        end case;
    end;
begin
    segment(data(3 downto 0), HEX0);
    segment(data(7 downto 4), HEX1);
    segment(data(11 downto 8), HEX2);
    segment(data(15 downto 12), HEX3);
    segment(data(19 downto 16), HEX4);
    segment(data(23 downto 20), HEX5);
end;
