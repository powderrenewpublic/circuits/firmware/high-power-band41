library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

package uart_mem is
    type t_uart_mem is array(natural range <>) of std_logic_vector(7 downto 0);
end package;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

use work.uart_mem.all;

entity uart_control is
    --generic (
    --    N_READ_WORDS : natural := 0;
    --    N_WRITE_WORDS : natural := 0
    --);
    port (
        clk_10mhz : in std_logic;
        rx : in std_logic;
        tx : out std_logic;

      --  read_data : in t_uart_mem(N_READ_WORDS-1 downto 0);
        --write_data : out t_uart_mem(N_WRITE_WORDS-1 downto 0);

        average_power : in unsigned(63 downto 0);
        shutdown_state : in std_logic_vector(7 downto 0);

        -- Asserted for one clock cycle when RESET_CODE is received from the UART
        serial_reset_received : out std_logic
    );
end uart_control;

architecture uart_control_arch of uart_control is
    signal din : std_logic_vector(7 downto 0);
    signal din_vld : std_logic;
    signal din_rdy : std_logic;

    signal dout : std_logic_vector(7 downto 0) := (others => '0');
    signal dout_vld : std_logic := '0';

    signal send_status : std_logic;

    type t_out_bytes is array(0 to 8) of std_logic_vector(7 downto 0);
    signal out_bytes : t_out_bytes;

    signal max_average_power : unsigned(63 downto 0) := (others => '0');
begin
    uart : entity work.UART
    generic map (
        CLK_FREQ => 10e6,
        BAUD_RATE => 9600,
        PARITY_BIT => "none",
        USE_DEBOUNCER => True
    )
    port map (
        CLK => clk_10mhz,
        RST => '0',
        UART_TXD => tx,
        UART_RXD => rx,
        DIN => din,
        DIN_VLD => din_vld,
        DIN_RDY => din_rdy,
        DOUT => dout,
        DOUT_VLD => dout_vld,
        FRAME_ERROR => open,
        PARITY_ERROR => open
    );

    out_bytes(8) <= shutdown_state;

    statistics : process(clk_10mhz)
    begin
        if clk_10mhz'event and clk_10mhz = '1' then
            if send_status = '1' then
                for i in 0 to 7 loop
                    out_bytes(i) <= std_logic_vector(max_average_power(i*8 + 7 downto i*8));
                end loop;

                max_average_power <= average_power;
            end if;

            if max_average_power < average_power then
                max_average_power <= average_power;
            end if;
        end if;
    end process;

--    transmit : process(clk_10mhz)
--    begin
--        if clk_10mhz'event and clk_10mhz = '1' then
--            if din_rdy = '1' then
--            end if;
--        end if;
--    end process;
--
--    receive : process(clk_10mhz)
--    begin
--        if clk_10mhz'event and clk_10mhz = '1' then
--            if dout_vld = '1' then
--            end if;
--        end if;
--    end process;

    status_send : process(clk_10mhz)
        variable send_byte : natural := out_bytes'length;
        variable prev_din_rdy : std_logic := '0';
    begin
        if clk_10mhz'event and clk_10mhz = '1' then
            din_vld <= '0';

            if send_byte < out_bytes'length then
                if din_rdy = '1' then
                    din <= out_bytes(send_byte);
                    din_vld <= '1';
                end if;

                if din_rdy = '0' and prev_din_rdy = '1' then
                    send_byte := send_byte + 1;
                end if;

                prev_din_rdy := din_rdy;
            end if;

            if send_status = '1' then
                send_byte := 0;
            end if;
        end if;
    end process;

    uart_reset : process(clk_10mhz)
        constant RESET_CODE : std_logic_vector(31 downto 0) := x"de_ad_be_ef";
        constant STATUS_CODE : std_logic_vector(31 downto 0) := x"ba_be_ca_fe";
        variable code_reg : std_logic_vector(31 downto 0) := (others => '0');
    begin
        if clk_10mhz'event and clk_10mhz = '1' then
            serial_reset_received <= '0';
            send_status <= '0';

            if dout_vld = '1' then
                code_reg := code_reg(23 downto 0) & dout;

                if code_reg = RESET_CODE then
                    serial_reset_received <= '1';
                elsif code_reg = STATUS_CODE then
                    send_status <= '1';
                end if;
            end if;
        end if;
    end process;
end;
