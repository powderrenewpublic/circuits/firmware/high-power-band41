library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity clock_ctrl is
    port (
        signal internal_clk_10mhz : in std_logic;
        signal external_clk_10mhz : in std_logic;
        
        signal master_clk_10mhz : out std_logic;
        signal pll_locked : out std_logic;
        signal external_clk_good : out std_logic
    );
end clock_ctrl;

architecture clock_ctrl_arch of clock_ctrl is
    signal areset : std_logic;
    signal clkswitch : std_logic;
    signal activeclock : std_logic;
    signal external_clk_good_reg : std_logic;
begin
    areset <= '0';
    external_clk_good <= external_clk_good_reg;

    pll_10mhz : entity work.pll_10mhz
    port map (
        areset => areset,
        clkswitch => clkswitch,
        inclk0 => internal_clk_10mhz,
        inclk1 => external_clk_10mhz,
        activeclock => activeclock,
        c0 => master_clk_10mhz,
        locked => pll_locked
    );

    -- Used to determine if the external 10 MHz reference clock is active
    pll_sense : entity work.pll_sense
    port map (
        areset => areset,
        inclk0 => external_clk_10mhz,
        c0 => open,
        locked => external_clk_good_reg
    );

    clock_switchover:
    process(internal_clk_10mhz)
        type clock_switchover_t is (
                INTERNAL,
                INTERNAL_TO_EXTERNAL,
                EXTERNAL,
                EXTERNAL_TO_INTERNAL);

        variable clock_switchover_state : clock_switchover_t := INTERNAL;
    begin
        if internal_clk_10mhz'event and internal_clk_10mhz = '1' then
            case clock_switchover_state is
                when INTERNAL =>
                    clkswitch <= '0';
                    if external_clk_good_reg = '1' then
                        clock_switchover_state := INTERNAL_TO_EXTERNAL;
                    end if;

                when INTERNAL_TO_EXTERNAL =>
                    clkswitch <= '1';
                    if activeclock = '1' then
                        clock_switchover_state := EXTERNAL;
                    end if;

                when EXTERNAL =>
                    clkswitch <= '0';
                    if external_clk_good_reg = '0' then
                        clock_switchover_state := EXTERNAL_TO_INTERNAL;
                    end if;

                when EXTERNAL_TO_INTERNAL =>
                    clkswitch <= '1';
                    if activeclock = '0' then
                        clock_switchover_state := INTERNAL;
                    end if;
            end case;
        end if;
    end process;
end;
